package com.mg.fai.constant;

public enum PriceUnit {
    INR("Indian Rupee"),USD("US Dollar"),CAD("Canadian Dollar");

    String description;

    PriceUnit(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return this.description;
    }
}
