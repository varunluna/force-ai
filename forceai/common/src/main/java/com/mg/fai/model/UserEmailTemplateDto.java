package com.mg.fai.model;

import lombok.Data;


@Data
public class UserEmailTemplateDto {
        private Integer id;
        private String body;
        private String subject;
        private String name;
}
