package com.mg.fai.model;

import com.mg.fai.constant.Operation;
import com.mg.fai.constant.RuleType;
import lombok.Data;

@Data
public class RuleDto {

    private Integer id;
    private SystemParametersDto parameter;
    private Operation operation;
    private String min;
    private String max;
    private RuleType ruleType;
}
