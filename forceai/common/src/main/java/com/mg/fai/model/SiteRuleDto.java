package com.mg.fai.model;

import com.mg.fai.constant.Event;
import lombok.Data;
import java.util.List;


@Data
public class SiteRuleDto {

    private Integer id;
    private String name;
    private String description;
    private List<RuleDto> rules;
    private Event event;
    private ActionDto action;
    private String ruleExpression;
    private SiteDto site;
    private String availableRule;
    private UserEmailTemplateDto UserEmailTemplate;
}
