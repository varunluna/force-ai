package com.mg.fai.model;

import com.mg.fai.constant.Connector;

import java.util.List;

public class SiteDto {

        private Integer id;
        private String name;
        private UserDto user;
        private Connector connector;
        private SiteTokenDto siteToken;
        private List<SiteRuleDto> siteRules;

    }
