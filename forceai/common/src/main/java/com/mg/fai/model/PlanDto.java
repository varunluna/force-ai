package com.mg.fai.model;

import com.mg.fai.constant.PriceUnit;
import lombok.Data;

@Data
public class PlanDto {

        private Integer id;
        private String name;
        private String description;
        private Integer siteSharingAllowed;
        private Integer ruleCreationAllowed;
        private Integer ruleExecutionAllowed;
        private Integer duration;
        private Double price;
        private PriceUnit units;
}
