package com.mg.fai.model;

import lombok.Data;


@Data
public class ActionDto {
    private Integer id;
    private String name;
}
