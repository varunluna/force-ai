package com.mg.fai.model;

import com.mg.fai.constant.SubscriptionStatus;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class UserSubscriptionDto {

    private Integer id;
    private PlanDto plan;
    private UserDto user;
    private Integer siteSharingAllowed;
    private Integer ruleCreationAllowed;
    private Integer ruleExecutionAllowed;
    private Integer siteSharingConsumed;
    private Integer ruleCreationConsumed;
    private Integer ruleExecutionConsumed;
    private Integer siteSharingAvailable;
    private Integer ruleCreationAvailable;
    private Integer ruleExecutionAvailable;
    private Timestamp startDate;
    private Timestamp endDate;
    private SubscriptionStatus subscriptionStatus;
}