package com.mg.fai.model;

import com.mg.fai.constant.DataType;
import lombok.Data;

@Data
public class SystemParametersDto {

    private Integer id;
    private String name;
    private DataType dataType;
}
