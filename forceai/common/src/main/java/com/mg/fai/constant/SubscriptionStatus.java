package com.mg.fai.constant;

public enum SubscriptionStatus {
    ACTIVE,EXPIRED,SUSPENDED;
}
