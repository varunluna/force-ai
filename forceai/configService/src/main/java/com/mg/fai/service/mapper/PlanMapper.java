package com.mg.fai.service.mapper;

import com.mg.fai.entity.PlanEntity;
import com.mg.fai.mapper.GenericMapper;
import com.mg.fai.model.PlanDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PlanMapper extends GenericMapper<PlanEntity, PlanDto> {

}
