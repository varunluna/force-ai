package com.mg.fai.entity;

import com.mg.fai.constant.Connector;

import javax.persistence.*;
import java.util.List;

@Entity
public class SiteEntity {

        @Id
        @GeneratedValue
        private Integer id;
        private String name;
        @ManyToOne
        private UserEntity user;
        @Enumerated(EnumType.STRING)
        private Connector connector;
        @OneToOne(cascade = CascadeType.ALL)
        private SiteTokenEntity siteToken;
        @OneToMany(mappedBy = "site")
        private List<SiteRuleEntity> siteRules;

    }
