package com.mg.fai.service;

import com.mg.fai.entity.UserEntity;
import com.mg.fai.exception.ForceAIException;
import com.mg.fai.model.UserDto;
import com.mg.fai.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface IUserService {

    public UserDto getUserById(Integer id) throws ForceAIException;

    public UserDto createUser(UserDto userDto) throws ForceAIException;

    public UserDto updateUser(UserDto userDto, Boolean create) throws ForceAIException;

    public void deleteUser(Integer id) throws ForceAIException;

    public UserDto getUserByName(String userName) throws ForceAIException;
}
