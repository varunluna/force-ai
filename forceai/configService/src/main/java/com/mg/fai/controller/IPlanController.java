package com.mg.fai.controller;

import com.mg.fai.exception.ForceAIException;
import com.mg.fai.model.PlanDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface IPlanController {

    @PostMapping("/plan")
    ResponseEntity createPlan(@RequestBody PlanDto planDto) throws ForceAIException;
}
