package com.mg.fai.service;

import com.mg.fai.entity.UserEntity;
import com.mg.fai.exception.ForceAIException;
import com.mg.fai.exception.message.ErrorMessage;
import com.mg.fai.model.UserDto;
import com.mg.fai.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements IUserService{
    @Autowired
    private UserRepository userRepositor;

    @Override
    public UserDto getUserById(Integer id) throws ForceAIException {
        Optional<UserEntity> userEntityOptional = userRepositor.findById(id);
        if (userEntityOptional.isEmpty())
            throw ForceAIException.someException(ErrorMessage.USER_NOT_PRESENT,new Object[]{id});
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(userEntityOptional.get(),userDto);
        return userDto;
    }

    @Override
    public UserDto createUser(UserDto userDto) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(userDto,userEntity);
        UserEntity userEntitySaved = userRepositor.save(userEntity);
        BeanUtils.copyProperties(userEntitySaved,userDto);
        return userDto;
    }

    @Override
    public UserDto updateUser(UserDto userDto, Boolean create) {
        Optional<UserEntity> UserOptional = userRepositor.findById(userDto.getId());
        if (UserOptional.isPresent()) {
            UserEntity userToSave = UserOptional.get();
            //userToSave.setId(userDto.getId());
            UserEntity userEntitySaved = userRepositor.save(userToSave);
            BeanUtils.copyProperties(userEntitySaved,userDto);
            return userDto;
        } else {
            if (create)
                return createUser(userDto);
            else
                return null;
        }
    }

    @Override
    public void deleteUser(Integer id) {
        userRepositor.deleteById(id);
    }

    @Override
    public UserDto getUserByName(String userName) {
        UserEntity userEntity = userRepositor.findByUsername(userName);
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(userEntity,userDto);
        return userDto;
    }
}
