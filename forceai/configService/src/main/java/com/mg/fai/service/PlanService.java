package com.mg.fai.service;

import com.mg.fai.entity.PlanEntity;
import com.mg.fai.exception.ForceAIException;
import com.mg.fai.model.PlanDto;
import com.mg.fai.repository.PlanRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanService implements  IPlanService{

    @Autowired
    private PlanRepository planRepository;

    @Override
    public PlanDto getPlanbyId(Integer planId) throws ForceAIException {
        return null;
    }

    @Override
    public List<PlanDto> getPlans() throws ForceAIException {
        return null;
    }

    @Override
    public PlanDto createPlan(PlanDto plan) throws ForceAIException {
        PlanEntity planEntity = new PlanEntity();
        BeanUtils.copyProperties(plan,planEntity);
        planEntity = planRepository.save(planEntity);
        BeanUtils.copyProperties(planEntity,plan);
        return plan;
    }
}
