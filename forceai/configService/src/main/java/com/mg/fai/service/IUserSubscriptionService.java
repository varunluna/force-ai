package com.mg.fai.service;

import com.mg.fai.exception.ForceAIException;
import com.mg.fai.model.UserSubscriptionDto;

public interface IUserSubscriptionService {
    UserSubscriptionDto getUserSubscriptionDetails(Integer userId) throws ForceAIException;

    UserSubscriptionDto subscribeToPlan(Integer userId,Integer planId) throws ForceAIException;
}
