package com.mg.fai.entity;


import com.mg.fai.constant.Connector;
import com.mg.fai.constant.Event;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class AvailableRuleEntity {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(unique = true)
    private String name;
    @Enumerated(EnumType.STRING)
    private Event event;
    private String ruleExpression;
    @OneToMany(fetch = FetchType.EAGER)
    private List<FixRuleEntity> rules;
    @ManyToOne
    private ActionEntity action;
    @OneToOne
    private DefaultEmailTemplateEntity defaultEmailTemplate;
    @Enumerated(EnumType.STRING)
    private Connector connector;
}