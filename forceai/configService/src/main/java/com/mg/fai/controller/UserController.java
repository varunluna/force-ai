
package com.mg.fai.controller;

import com.mg.fai.exception.ForceAIException;
import com.mg.fai.exception.message.ErrorMessage;
import com.mg.fai.model.UserDto;
import com.mg.fai.model.UserSubscriptionDto;
import com.mg.fai.service.IUserService;
import com.mg.fai.service.IUserSubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController implements IUserController{

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserSubscriptionService userSubscriptionService;

    @Override
    public ResponseEntity<UserDto> saveUser(UserDto userDto) throws ForceAIException {
        UserDto user = userService.createUser(userDto);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<UserDto>> getUser() throws ForceAIException {
        throw ForceAIException.someException(ErrorMessage.NOT_IMPLEMENTED,null);
    }

    @Override
    public ResponseEntity<UserDto> getUser(Integer userId) throws ForceAIException {
        UserDto userDto = userService.getUserById(userId);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserSubscriptionDto> getSubscriptionDetails(Integer userId) throws ForceAIException {
        return new ResponseEntity<>(userSubscriptionService.getUserSubscriptionDetails(userId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserSubscriptionDto> subscribeToPlan(Integer userId, Integer planId) throws ForceAIException{
        return new ResponseEntity<>(userSubscriptionService.subscribeToPlan(userId,planId), HttpStatus.OK);
    }
}
