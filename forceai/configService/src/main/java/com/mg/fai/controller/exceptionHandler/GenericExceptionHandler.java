package com.mg.fai.controller.exceptionHandler;

import com.mg.fai.exception.ForceAIException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ForceAIException.class)
    public ResponseEntity<Object> handleException(

            ForceAIException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("ExceptionMessage", ex.getMessage());
        body.put("FA-INFO",ex.toFormat());
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

}
