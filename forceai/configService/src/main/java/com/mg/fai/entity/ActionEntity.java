package com.mg.fai.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class ActionEntity {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
}
