package com.mg.fai.controller;

import com.mg.fai.entity.UserSubscriptionEntity;
import com.mg.fai.exception.ForceAIException;
import com.mg.fai.model.UserDto;
import com.mg.fai.model.UserSubscriptionDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface IUserController {

    @PostMapping("/user")
    public ResponseEntity<UserDto> saveUser(@RequestBody UserDto userDto) throws ForceAIException;
    @GetMapping("/user")
    public ResponseEntity<List<UserDto>> getUser() throws ForceAIException;
    @GetMapping("/user/{userId}")
    public ResponseEntity<UserDto> getUser(@PathVariable Integer userId) throws ForceAIException;
    @GetMapping("/user/plan/{userId}")
    public ResponseEntity<UserSubscriptionDto> getSubscriptionDetails(@PathVariable Integer userId) throws ForceAIException;
    @GetMapping("/user/plan/{userId}/{planId}")
    public ResponseEntity<UserSubscriptionDto> subscribeToPlan(@PathVariable Integer userId , @PathVariable Integer planId) throws ForceAIException;
}
