package com.mg.fai.controller;

import com.mg.fai.entity.UserSubscriptionEntity;
import com.mg.fai.exception.ForceAIException;
import com.mg.fai.model.SiteDto;
import com.mg.fai.model.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface ISiteController {

    @PostMapping("/site/{userId}")
    public SiteDto addSite(@PathVariable Integer userId, @RequestBody SiteDto siteDto);
    @DeleteMapping("/site/{siteId}")
    public void deleteSite(@PathVariable Integer siteId) ;
    @GetMapping("/site/{siteId}")
    public SiteDto getSite(@PathVariable Integer siteId) ;
    @GetMapping("/siteByToken/{siteToken}")
    public SiteDto getSiteByToken(@PathVariable String siteToken);
}
