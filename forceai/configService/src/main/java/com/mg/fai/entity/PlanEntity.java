package com.mg.fai.entity;

import com.mg.fai.constant.PriceUnit;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlanEntity {

        @Id
        @GeneratedValue
        private Integer id;
        private String name;
        private String description;
        private Integer siteSharingAllowed;
        private Integer ruleCreationAllowed;
        private Integer ruleExecutionAllowed;
        private Integer duration;
        private Double price;
        @Enumerated(EnumType.STRING)
        private PriceUnit units;
}
