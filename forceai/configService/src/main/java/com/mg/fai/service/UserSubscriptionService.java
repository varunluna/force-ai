package com.mg.fai.service;

import com.mg.fai.entity.UserSubscriptionEntity;
import com.mg.fai.exception.ForceAIException;
import com.mg.fai.exception.message.ErrorMessage;
import com.mg.fai.model.UserDto;
import com.mg.fai.model.UserSubscriptionDto;
import com.mg.fai.repository.PlanRepository;
import com.mg.fai.repository.UserRepository;
import com.mg.fai.repository.UserSubscriptionRepository;
import com.mg.fai.service.mapper.UserSubscriptionMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserSubscriptionService  implements  IUserSubscriptionService{

    @Autowired
    private IUserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private UserSubscriptionRepository userSubscriptionRepository;

    @Autowired(required = false)
    private UserSubscriptionMapper userSubscriptionMapper;

    @Override
    public UserSubscriptionDto getUserSubscriptionDetails(Integer userId) throws ForceAIException {
        Optional<UserSubscriptionEntity> userSubscriptionEntity = userSubscriptionRepository.findByUserId(userId);
        if (userSubscriptionEntity.isEmpty())
            throw ForceAIException.someException(ErrorMessage.NO_DATA_FOUND,new Object[]{userId});
        UserSubscriptionDto userSubscriptionDto = new UserSubscriptionDto();
        //userSubscriptionDto = userSubscriptionMapper.entityToModel(userSubscriptionEntity.get());
        BeanUtils.copyProperties(userSubscriptionEntity.get(),userSubscriptionDto);
        return userSubscriptionDto;
    }

    @Override
    public UserSubscriptionDto subscribeToPlan(Integer userId,Integer planId) throws ForceAIException{
        //if user exists give him plan other wise throw exception
        UserDto userDto = userService.getUserById(userId);
        Optional<UserSubscriptionEntity> userSubscriptionEntity = userSubscriptionRepository.findByUserId(userId);
        if(userSubscriptionEntity.isPresent())
            throw ForceAIException.someException(ErrorMessage.SUBSCRIPTION_ALREADY_THERE,new Object[]{userSubscriptionEntity.get().getPlan().getName()});
        UserSubscriptionEntity userSubscriptionEntityToSave = prepareSubscriptionData(userId,planId);
        userSubscriptionEntityToSave = userSubscriptionRepository.save(userSubscriptionEntityToSave);
        UserSubscriptionDto userSubscriptionDto = new UserSubscriptionDto();
        BeanUtils.copyProperties(userSubscriptionEntity,userSubscriptionDto);
        return userSubscriptionDto;
    }

    private UserSubscriptionEntity prepareSubscriptionData(Integer userId, Integer planId) {
        UserSubscriptionEntity userSubscriptionEntity = new UserSubscriptionEntity();
        userSubscriptionEntity.setUser(userRepository.getOne(userId));
        userSubscriptionEntity.setPlan(planRepository.getOne(planId));
        return userSubscriptionEntity;
    }
}
