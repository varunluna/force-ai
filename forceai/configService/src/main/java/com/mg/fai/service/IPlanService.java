package com.mg.fai.service;

import com.mg.fai.exception.ForceAIException;
import com.mg.fai.model.PlanDto;

import java.util.List;

public interface IPlanService {

    PlanDto getPlanbyId(Integer planId) throws ForceAIException;
    List<PlanDto> getPlans() throws ForceAIException;
    PlanDto createPlan(PlanDto plan) throws ForceAIException;
}
