package com.mg.fai.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class SiteTokenEntity {

    @Id
    @GeneratedValue
    private Integer id;
    private String siteToken;
}
