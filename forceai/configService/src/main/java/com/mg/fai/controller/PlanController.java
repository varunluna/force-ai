package com.mg.fai.controller;

import com.mg.fai.exception.ForceAIException;
import com.mg.fai.model.PlanDto;
import com.mg.fai.service.IPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlanController implements  IPlanController{

    @Autowired
    private IPlanService planService;

    @Override
    public ResponseEntity<PlanDto> createPlan(PlanDto planDto) throws ForceAIException {
        return new ResponseEntity<>(planService.createPlan(planDto), HttpStatus.OK);
    }
}
