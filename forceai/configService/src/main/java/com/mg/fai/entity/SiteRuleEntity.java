package com.mg.fai.entity;

import com.mg.fai.constant.Event;

import javax.persistence.*;
import java.util.List;

import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
@Data
public class SiteRuleEntity {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String description;
    @OneToMany
    @Cascade(CascadeType.ALL)
    private List<RuleEntity> rules;
    @Enumerated(EnumType.STRING)
    private Event event;
    @ManyToOne
    private ActionEntity action;
    private String ruleExpression;
    @ManyToOne
    private SiteEntity site;
    private String availableRule;
    @OneToOne
    private UserEmailTemplateEntity UserEmailTemplate;
}
