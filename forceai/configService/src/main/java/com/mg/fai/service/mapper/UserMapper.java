package com.mg.fai.service.mapper;

import com.mg.fai.entity.UserEntity;
import com.mg.fai.mapper.GenericMapper;
import com.mg.fai.model.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")

public interface UserMapper extends GenericMapper<UserEntity, UserDto> {
}
