package com.mg.fai.service.mapper;

import com.mg.fai.entity.UserSubscriptionEntity;
import com.mg.fai.mapper.GenericMapper;
import com.mg.fai.model.UserSubscriptionDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserSubscriptionMapper extends GenericMapper<UserSubscriptionEntity, UserSubscriptionDto> {
}
