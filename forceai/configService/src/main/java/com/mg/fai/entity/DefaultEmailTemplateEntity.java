package com.mg.fai.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class DefaultEmailTemplateEntity {
        @Id
        @GeneratedValue
        private Integer id;
        private String body;
        private String subject;
        private String name;
}
