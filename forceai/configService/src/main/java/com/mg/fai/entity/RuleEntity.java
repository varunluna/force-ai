package com.mg.fai.entity;

import com.mg.fai.constant.Operation;
import com.mg.fai.constant.RuleType;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class RuleEntity {

    @Id
    @GeneratedValue
    private Integer id;
    @OneToOne
    private SystemParametersEntity parameter;
    @Enumerated(EnumType.STRING)
    private Operation operation;
    private String min;
    private String max;
    @Enumerated(EnumType.STRING)
    private RuleType ruleType;
}
