package com.mg.fai.repository;

import com.mg.fai.entity.UserSubscriptionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserSubscriptionRepository extends JpaRepository<UserSubscriptionEntity,Integer> {

    Optional<UserSubscriptionEntity> findByUserId(Integer userId);
}
