package com.mg.fai.entity;

import com.mg.fai.constant.DataType;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class SystemParametersEntity {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    @Enumerated(EnumType.STRING)
    private DataType dataType;
}
