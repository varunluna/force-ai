package com.mg.fai.entity;

import com.mg.fai.constant.SubscriptionStatus;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.sql.Timestamp;

@Entity
@Data
public class UserSubscriptionEntity {

    @Id
    @GeneratedValue
    private Integer id;
    @OneToOne
    private PlanEntity plan;
    @OneToOne
    private UserEntity user;
    private Integer siteSharingAllowed;
    private Integer ruleCreationAllowed;
    private Integer ruleExecutionAllowed;
    private Integer siteSharingConsumed;
    private Integer ruleCreationConsumed;
    private Integer ruleExecutionConsumed;
    private Integer siteSharingAvailable;
    private Integer ruleCreationAvailable;
    private Integer ruleExecutionAvailable;
    private Timestamp startDate;
    private Timestamp endDate;
    private SubscriptionStatus subscriptionStatus;
}