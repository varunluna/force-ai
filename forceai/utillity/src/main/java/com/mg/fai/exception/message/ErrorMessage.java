package com.mg.fai.exception.message;

public enum ErrorMessage {
    NO_DATA_FOUND(1234,"No Element Found for {0}"),
    NOT_IMPLEMENTED(100,"This functionality is not available"),
    SUBSCRIPTION_ALREADY_THERE(101,"User already have subscription {0}"),
    USER_NOT_PRESENT(404,"User not present for {0}");

    String errorMessage;
    double errorCode;

    ErrorMessage(double errorCode, String errorMessage)
    {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public double getErrorCode() {
        return errorCode;
    }
}
