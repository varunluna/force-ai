package com.mg.fai.mapper;

import org.mapstruct.Mapper;

@Mapper(componentModel = "Spring")
public interface GenericMapper<E,M> {

    E modelToEntity( M m);
    M entityToModel(E e);
}
