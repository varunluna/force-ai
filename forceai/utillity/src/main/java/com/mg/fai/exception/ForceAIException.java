package com.mg.fai.exception;

import com.mg.fai.exception.message.ErrorMessage;

public class ForceAIException extends  Exception{

    protected final double code;
    protected final String message;

    protected ForceAIException(Exception e, ErrorMessage msg, Object... msgArgs)
    {
        this(e==null ? new Exception() : e,msg.getErrorCode(),process(msg,msgArgs));
    }

    protected ForceAIException(Exception e, double code, String msg)
    {
        super(msg,e);
        this.code = code;
        this.message = msg;
    }

    protected static String process(ErrorMessage msg, Object[] msgArgs) {
        if (msgArgs ==null || msgArgs.length ==0)
            return msg.getErrorMessage();
        if (msg.getErrorMessage() == null)
            return null;
        return process(msg.getErrorMessage(),msgArgs);
    }

    protected static String process(String errorMsg, Object [] msgArgs)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i< msgArgs.length ; i++)
        {
            int startIndex = sb.indexOf("{"+i+"}");
            if (startIndex!= -1)
            {
                sb.replace(startIndex,startIndex+3,String.valueOf(msgArgs[i]));
            }
        }
        return sb.toString();
    }

    public static ForceAIException someException(ErrorMessage errorMessage, Object [] msgArgs)
    {
        return someException(null,errorMessage,msgArgs);
    }

    public static ForceAIException someException(Exception e, ErrorMessage errorMessage, Object [] msgArgs)
    {
        return new ForceAIException(e,errorMessage,msgArgs);
    }

    public String toFormat()
    {
        return "FA code : "+code+" , FA message : "+message;
    }
}
